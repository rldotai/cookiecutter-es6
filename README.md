# ES6 + Babel + WebPack + React + Redux + Mocha

There are a panoply of tools available for JavaScript, but setting them up is time consuming.
However, most of the setup is just boilerplate, so we can use [Cookiecutter](https://github.com/audreyr/cookiecutter) to handle this for us.

Run cookiecutter via:

```
# Clone the repo, and then cookiecutter 
# git clone https://gitlab.com/rldotai/cookiecutter-es6
cookiecutter cookiecutter-es6

# Get the template from its repo
cookiecutter https://gitlab.com/rldotai/cookiecutter-es6

# Specify an alternate directory to setup the project in
cookiecutter cookiecutter-es6 -o /path/to/project/parent/directory
```

You will then specify some information about the project on the command line.

The result will be a directory named according to what you specified for the project's repo (default: `my-es6-project-repo`), setup for a package with the name you gave for the project's name (default: `my-es6-project`).

# ES6 Project Setup

We have decided to program in JavaScript, and are even so reckless as to try to use EcmaScript 2015 via `babel` and `webpack`.

We would also like to do testing, so after some perfunctory googling, have elected to use `mocha`.

How can we make it all work?

## Webpack

We use `webpack` along with the `copy-webpack-plugin` to build the code and move the files to the `./html` directory in order to be able to run our code from the browser.
Note that we target a library instead of building an app.
The library's name will be `main`, located at `./build/main.js`
Not compiling as a library is as simple as removing the `library: main` line in the `webpack.config.js` file.

## NPM Scripts

We have a few scripts for automating tasks without needing to use a Makefile.

- build: compile the code and place it into the `build` directory, and afterwards copy it to the `html` directory.
- clean: remove everything from the `build` directory.
- start: run the development server
- test: run the tests in the `test` directory, compiling them to use ES6 in their own code as well.
- watch: watch the directories, running `build` when a file is changed.

## Mocha

We have to compile the code in order to use ES6 features, but thankfully that just entails modifying the command line argument:

```bash
mocha test  --compilers js:babel-core/register
```


# TODO

- [ ] Post-install hook for configuring github username.