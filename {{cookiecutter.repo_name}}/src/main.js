import 'babel-polyfill';
import React from 'react'
import ReactDOM from 'react-dom'
import { createStore } from 'redux'
import { Provider, connect } from 'react-redux'


// React component
class Counter extends React.Component {
    static propTypes = {
        value: React.PropTypes.number.isRequired,
        onIncreaseClick: React.PropTypes.func.isRequired
    }

    render() {
        const { value, onIncreaseClick } = this.props
        return (
            <div>
        <span>{value}</span>
        <button onClick={onIncreaseClick}>Increase</button>
      </div>
        );
    }
}

// Action
const increaseAction = { type: 'increase' };

// Reducer
function counter(state = { count: 0 }, action) {
    const count = state.count
    switch (action.type) {
        case 'increase':
            return { count: count + 1 }
        default:
            return state
    }
};

// Store
const store = createStore(counter);

// Map Redux state to component props
function mapStateToProps(state) {
    return {
        value: state.count
    }
};

// Map Redux actions to component props
function mapDispatchToProps(dispatch) {
    return {
        onIncreaseClick: () => dispatch(increaseAction)
    }
}

// Connected Component
const App = connect(
    mapStateToProps,
    mapDispatchToProps
)(Counter);

const rootElement = document.getElementById('root');

ReactDOM.render(<Provider store={store}><App /></Provider>, rootElement);