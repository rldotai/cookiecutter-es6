# {{ cookiecutter.project_name }}

## Quick Start

Install development dependencies

```bash
npm install
```

Run Webpack's Development Server, recompiling and reloading the browser when
relevant files change:

```bash
npm run start
```

Watch files and recompile on changes

```bash
npm watch
```

Run tests with Mocha.

```bash
npm test
```

Clean the `build` directory (remove all files).

```bash
npm clean
```